#pragma once

#include <stack>
#include <string>
#include <vector>
#include <iostream>
#include "Operator.h"
using namespace std;

class Evaluator
{
public:
	int eval(string s);
	// Evaluates an input infix expression and returns an integer value for the fully evaluated expression.

	stack<int> getNumberStack() { return numberStack; }
private:
	stack<int> numberStack;
	stack<Operator> operatorStack;
	string expression;
	static const string POSSIBLE_OP; // a list of all possible characters that can be used to make an operator

	bool isOperator(char op);
	// returns true if the given character is a valid operator

	bool isNegative(const string& expression, int index);
	// returns true if the character at the given index in the given expression can be interpreted as a negative number

	bool isLeftParens(char op);  // booleans that return true if the given character or Operator is a parenthesis
	bool isLeftParens(Operator op);
	bool isRightParens(char op);

	const Operator getOperator(const string& op);
	// creates and returns an Operator object from the given string

	void processOperator(const string& op);
	// pushes operators onto the stack or evaluates expressions according to the rules of precedence

	void evaluate();
	// pops values from the operator and number stacks to evaluate expressions.  The result is pushed back onto the number stack.

	void evalBinary(int lhs, int rhs, Operator& op);
	// evaluates a binary operator and pushes the result onto the number stack.

	void evalUnary(int rhs, Operator& op);
	// evaluates a unary operator and pushes the result onto the number stack.
};