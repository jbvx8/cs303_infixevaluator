#include "Operator.h"

const Operator Operator::OR = Operator("||", 1, true);
const Operator Operator::AND = Operator("&&", 2, true);
const Operator Operator::EQUAL = Operator("==", 3, true);
const Operator Operator::NOT_EQUAL = Operator("!=", 3, true);
const Operator Operator::GREATER_THAN = Operator(">", 4, true);
const Operator Operator::GREATER_OR_EQUAL = Operator(">=", 4, true);
const Operator Operator::LESS_THAN = Operator("<", 4, true);
const Operator Operator::LESS_OR_EQUAL = Operator("<=", 4, true);
const Operator Operator::PLUS = Operator("+", 5, true);
const Operator Operator::MINUS = Operator("-", 5, true);
const Operator Operator::MULTIPLY = Operator("*", 6, true);
const Operator Operator::DIVIDE = Operator("/", 6, true);
const Operator Operator::MODULO = Operator("%", 6, true);
const Operator Operator::POWER = Operator("^", 7, true);
const Operator Operator::PREFIX_DECREMENT = Operator("--", 8, false);
const Operator Operator::PREFIX_INCREMENT = Operator("++", 8, false);
const Operator Operator::NOT = Operator("!", 8, false);
const Operator Operator::LEFT_PARENS = Operator("(", -1, false);
const Operator Operator::LEFT_BRACKET = Operator("[", -1, false);
const Operator Operator::LEFT_BRACE = Operator("{", -1, false);
