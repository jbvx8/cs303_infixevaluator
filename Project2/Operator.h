#pragma once
#include <string>
using namespace std;

class Operator
{
public:
	// Can only create static Operators with set values for symbol, precedence, and whether it is binary or not
	static const Operator OR;
	static const Operator AND;
	static const Operator EQUAL;
	static const Operator NOT_EQUAL;
	static const Operator GREATER_THAN;
	static const Operator GREATER_OR_EQUAL;
	static const Operator LESS_THAN;
	static const Operator LESS_OR_EQUAL;
	static const Operator PLUS;
	static const Operator MINUS;
	static const Operator MULTIPLY;
	static const Operator DIVIDE;
	static const Operator MODULO;
	static const Operator POWER;
	static const Operator PREFIX_DECREMENT;
	static const Operator PREFIX_INCREMENT;
	static const Operator NOT;
	static const Operator LEFT_PARENS;
	static const Operator LEFT_BRACKET;
	static const Operator LEFT_BRACE;

	string getSymbol() { return symbol; }
	int getPrecedence() { return precedence; }
	bool isBinary() { return binary; }
	

private:
	string symbol; // e.g. "+"
	int precedence;  // for order of operations
	bool binary;  // true if the operator is binary, false if it is unary or parens

	Operator(string s, int prec, bool b)
	{
		this->symbol = s;
		this->precedence = prec;
		this->binary = b;
	}

};