// Jackie Batson
// CS303
// Project 2
// Git URL : https://bitbucket.org/jbvx8/project2


#include "Evaluator.h"
#include <iostream>
using namespace std;

void test(string expression, int expected);
// Compares expected and generated values for the given expression

int main()
{
	test("1+2", 3);
	// test order of operations
	test("1+2*3", 7);
	test("1*2+3", 5);
	test("2+2^3*3", 26);
	// test order of operations with parentheses
	test("(2+2)^3*4", 256);
	test("2^(3*2)", 64);

	// test booleans
	test("5<6", 1);
	test("!1", 0);
	test("(2+3) < (4+1)", 0);
	test("(2+3) <= (4+1)", 1);

	// test negatives
	test("1+-3", -2);
	test("-5 < -6", 0);

	// test unary
	test("++5", 6);
	test("--2", 1);
	test("++++2", 4);
	test("++++2-5*(3^2)", -41);

	//Evaluator eval;
	//try
	//{
	//	int result = eval.eval("1+2");
	//}
	//catch (exception e)
	//{
	//	system("pause");
	//	return EXIT_FAILURE;  // program exits if an exception is thrown.  The pause is to display the error message.
	//}
	
	return 0;
}

void test(string expression, int expected)
{
	Evaluator eval;
	int result = eval.eval(expression);
	if (result == expected)
	{
		cout << expression << ": " << result << ": Pass" << endl;
	}
	else
	{
		cout << expression << ": Fail. Expected " << expected << " got " << result << endl;
	}
}