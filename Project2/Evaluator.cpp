#include "Evaluator.h"
#include "Operator.h"

const string Evaluator::POSSIBLE_OP = "|&=!><+-*/%^()[]{}";  // string of possible characters that can be used to construct an operator

int Evaluator::eval(string s)
{
	string numString = "";
	string opString = "";
	int count = 0, numSinceOp = 0; // numSinceOp keeps track of the number of operands that have been read without an operator being read.
	
	for (int i = 0; i < s.size(); i++)
	{
		if (s[i] == ' ')
		{
			continue;  // ignore whitespace
		}
		else if (s[i] == '-')  // minuses are treated specially since they could refer to negatives
		{
			if (i < (s.size() - 1))
			{
				if (isdigit(s[i + 1]) && isNegative(s, i)) // negative operators cannot be separated from operand by space
				{
					if (!opString.empty())  // if there is a value in the operator string, process the character as an operator so that the - can be interpreted as a negative, not an operator
					{
						processOperator(opString);
						numSinceOp = 0;
						opString = "";
						count = 0;
					}
					numString += s[i];  // negative numbers have the - operator added to the temporary number string
					continue;
				}
			}
			opString += s[i]; // append the - onto the opString if it is not a negative
			count++;
			if (count == 2) // operators can have two characters at most
			{
				try
				{
					processOperator(opString);
					numSinceOp = 0;
					opString = "";
					count = 0;
				}
				catch (exception e)
				{
					cerr << e.what() << i << endl;
					throw;
				}
			}
		}
		else if (isdigit(s[i]))
		{
			if (!opString.empty())  // process any read but unprocessed operators before proceeding with a digit
			{
				try
				{
					processOperator(opString);  
					numSinceOp = 0;
					opString = "";
					count = 0;
				}
				catch (exception e)
				{
					cerr << e.what() << i << endl;
					throw;
				}
			}
			numString += s[i];
			if (i == s.size() - 1 || i < s.size() - 1 && !isdigit(s[i + 1]))
			{
				int number = stoi(numString);
				numberStack.push(number);
				numSinceOp++;
				if (numSinceOp > 1) // if two operands have been read in a row
				{
					numSinceOp = 0;
					cerr << "Can't have two operands in a row at character " << i << endl;
					throw exception();
				}
				numString = "";
				while (!operatorStack.empty() && !operatorStack.top().isBinary() && !isLeftParens(operatorStack.top()))
				{
					try
					{
						evaluate(); 
					}
					catch (exception e)
					{
						cerr << e.what() << i << endl;
						throw;
					}
				}
			}
			continue;
		}
		else if (isOperator(s[i]))
		{
			
			if (!numString.empty())
			{
				int number = stoi(numString);
				numberStack.push(number);
				numSinceOp++;
				if (numSinceOp > 1)
				{
					numSinceOp = 0;
					cerr << "Can't have two operands in a row at character " << i << endl;
					throw exception();
				}
				numString = "";
				while (!operatorStack.empty() && !operatorStack.top().isBinary())
				{
					try
					{
						evaluate();
					}
					catch (exception e)
					{
						cerr << e.what() << i << endl;
						throw;
					}
				}
			}

			if (isRightParens(s[i]))
			{
				if (i == 0)
				{
					cerr << "Can't begin an expression with closing parenthesis at character " << i << endl;
					throw exception();
				}
				try
				{
					evaluate();  // start evaluating when a ) is read.
				}
				catch (exception e)
				{
					cerr << e.what() << endl;
					throw;
				}
				if (!operatorStack.empty() && isLeftParens(operatorStack.top()))
				{
					operatorStack.pop(); // remove opening parentheses
					continue;
				}
				else // Opening parentheses should be on top of the operator stack now.  If not, there are mismatched parentheses.
				{
					cerr << "No opening parenthesis matches the closing parenthesis at character " << i << endl;
					throw exception();
				}
				continue;
			}
			else
			{
				opString += s[i];
				count++;  
				if (count == 2 || (i < (s.size() - 1) && (!isOperator(s[i + 1]) || isLeftParens(s[i + 1]) || isRightParens(s[i + 1])) || i == (s.size() - 1)))
					// process the operator if there are two characters, the next character is not an operator or is parentheses, or is the last character in the string
				{
					try
					{
						processOperator(opString);
						numSinceOp = 0;
						opString = "";
						count = 0;
					}
					catch (exception e)
					{
						cerr << e.what() << i << endl;
						throw;
					}

				}
				continue;
			}
		}
		else
		{
			cerr << "Invalid character at position " << i << endl;
			throw exception();
		}

	}
	// clear stacks for result
	while (!operatorStack.empty())
	{
		try
		{
			evaluate();
		}
		catch (exception e)
		{
			cerr << e.what() << endl;
			throw;
		}
	}
	return numberStack.top();
}




bool Evaluator::isNegative(const string& expression, int index)
{
	if (isdigit(expression[index + 1]))
	{ 
		// a negative must be immediately followed by a digit, cannot be immediately preceded by a digit or minus symbol
		if ((index > 0  && (!isdigit(expression[index-1]) && expression[index-1] != '-')) || index == 0)
		return true;
	}
	return false;
}


void Evaluator::processOperator(const string& op)
{
	
	Operator oper = getOperator(op); 

	if (operatorStack.empty() || op == "(" || op == "[" || op == "{" || oper.getPrecedence() > operatorStack.top().getPrecedence() || !oper.isBinary())
	{
		operatorStack.push(oper); // if the operator stack is empty, the operator is opening parentheses, higher precedence than top operator stack, or unary, push it immediately to operator stack
	}
	else
	{
		while (!operatorStack.empty() && oper.getPrecedence() <= operatorStack.top().getPrecedence())
		{
			evaluate();  // otherwise evaluate the higher precedence operators before pushing this one.
		}
		operatorStack.push(oper);
	}
}

void Evaluator::evaluate()
{
	Operator oldOp = operatorStack.top(); // operator stack should not be empty here
	if (oldOp.isBinary())
	{
		if (numberStack.empty())
		{
			throw exception("A binary operator cannot precede an operand.");
		}
		else if (numberStack.size() < 2)
		{
			throw exception("Not enough operands for binary operator.");
		}
		int rhs = numberStack.top();
		numberStack.pop();
		int lhs = numberStack.top();
		numberStack.pop();
		operatorStack.pop();
		if (oldOp.getSymbol() == "/" && rhs == 0)
		{
			throw exception("Divide by zero.");
		}
		evalBinary(lhs, rhs, oldOp);
	}
	else
	{
		if (!numberStack.empty())
		{
			int rhs = numberStack.top();
			numberStack.pop();
			operatorStack.pop();
			evalUnary(rhs, oldOp);
		}
	}
}

bool Evaluator::isOperator(char op)
{
	for (int i = 0; i < POSSIBLE_OP.size(); i++)
	{
		if (POSSIBLE_OP[i] == op)  // iterate through string of possible operators
		{
			return true;
		}
	}
		return false;
}

bool Evaluator::isLeftParens(char op)
{
	if (op == '(' || op == '[' || op == '{')
	{
		return true;
	}
	return false;
}

bool Evaluator::isLeftParens(Operator op)
{
	if (op.getSymbol() == "(" || op.getSymbol() == "[" || op.getSymbol() == "{")
	{
		return true;
	}
	return false;
}

bool Evaluator::isRightParens(char op)
{
	if (op == ')' || op == ']' || op == '}')
	{
		return true;
	}
	return false;
}


void Evaluator::evalBinary(int lhs, int rhs, Operator& op)
{
	if (op.getSymbol() == "||")
	{
		numberStack.push(lhs || rhs);
	}
	else if (op.getSymbol() == "&&")
	{
		numberStack.push(lhs && rhs);
	}
	else if (op.getSymbol() == "==")
	{
		numberStack.push(lhs == rhs);
	}
	else if (op.getSymbol() == "!=")
	{
		numberStack.push(lhs != rhs);
	}
	else if (op.getSymbol() == ">")
	{
		numberStack.push(lhs > rhs);
	}
	else if (op.getSymbol() == ">=")
	{
		numberStack.push(lhs >= rhs);
	}
	else if (op.getSymbol() == "<")
	{
		numberStack.push(lhs < rhs);
	}
	else if (op.getSymbol() == "<=")
	{
		numberStack.push(lhs <= rhs);
	}
	else if (op.getSymbol() == "+")
	{
		numberStack.push(lhs + rhs);
	}
	else if (op.getSymbol() == "-")
	{
		numberStack.push(lhs - rhs);
	}
	else if (op.getSymbol() == "*")
	{
		numberStack.push(lhs * rhs);
	}
	else if (op.getSymbol() == "/")
	{
		numberStack.push(lhs / rhs);
	}
	else if (op.getSymbol() == "%")
	{
		numberStack.push(lhs % rhs);
	}
	else if (op.getSymbol() == "^")
	{
		numberStack.push(pow(lhs, rhs));
	}
}

void Evaluator::evalUnary(int rhs, Operator& op)
{
	if (op.getSymbol() == "--")
	{
		numberStack.push(--rhs);
	}
	else if (op.getSymbol() == "++")
	{
		numberStack.push(++rhs);
	}
	else if (op.getSymbol() == "!")
	{
		numberStack.push(!rhs);
	}
}

const Operator Evaluator::getOperator(const string& op)
{
	if (op == "||")
	{
		return Operator::OR;
	}
	else if (op == "&&")
	{
		return Operator::AND;
	}
	else if (op == "==")
	{
		return Operator::EQUAL;
	}
	else if (op == "!=")
	{
		return Operator::NOT_EQUAL;
	}
	else if (op == ">")
	{
		return Operator::GREATER_THAN;
	}
	else if (op == ">=")
	{
		return Operator::GREATER_OR_EQUAL;
	}
	else if (op == "<")
	{
		return Operator::LESS_THAN;
	}
	else if (op == "<=")
	{
		return Operator::LESS_OR_EQUAL;
	}
	else if (op == "+")
	{
		return Operator::PLUS;
	}
	else if (op == "-")
	{
		return Operator::MINUS;
	}
	else if (op == "*")
	{
		return Operator::MULTIPLY;
	}
	else if (op == "/")
	{
		return Operator::DIVIDE;
	}
	else if (op == "%")
	{
		return Operator::MODULO;
	}
	else if (op == "^")
	{
		return Operator::POWER;
	}
	else if (op == "--")
	{
		return Operator::PREFIX_DECREMENT;
	}
	else if (op == "++")
	{
		return Operator::PREFIX_INCREMENT;
	}
	else if (op == "!")
	{
		return Operator::NOT;
	}
	else if (op == "(")
	{
		return Operator::LEFT_PARENS;
	}
	else if (op == "[")
	{
		return Operator::LEFT_BRACKET;
	}
	else if (op == "{")
	{
		return Operator::LEFT_BRACE;
	}
	else
	{
		throw exception("Invalid operator at character ");
	}


}